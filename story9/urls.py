from django.urls import path
from .views import signUpView
from . import views

app_name = 'story9'

urlpatterns = [
    path('signup', signUpView.as_view(), name='signup'),
    path('hi', views.hi , name='hi'),
]
