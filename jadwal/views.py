from django.shortcuts import render, redirect
from .models import Jadwal
from .forms import JadwalForm

# Create your views here.
def add_jadwal(request):
    if request.method == 'POST':
        form = JadwalForm(request.POST)
        if form.is_valid():
            jadwal = form.save()
            jadwal.save()
            return redirect('jadwal:view_jadwal')
    else:
        form = JadwalForm()
        
    return render(request,'add_jadwal.html',{'form' : form})

def view_jadwal(request):
    semua_jadwal = Jadwal.objects.order_by('Subject')
    return render(request,'view_jadwal.html',{'semua_jadwal': semua_jadwal})

def delete_jadwal(request,id):
    Jadwal.objects.get(id=id).delete()
    return redirect('jadwal:view_jadwal')

def detail(request,id):
    detail = Jadwal.objects.get(id=id)
    return render(request,'detail.html',{'detail': detail})
