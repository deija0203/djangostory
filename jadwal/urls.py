from django.urls import path
from . import views

app_name = 'jadwal'

urlpatterns = [
    path('add/', views.add_jadwal, name='add_jadwal'),
    path('view_jadwal/', views.view_jadwal, name='view_jadwal'),
    path('delete/<id>', views.delete_jadwal, name='delete_jadwal'),
    path('detail/<id>', views.detail, name='detail'),
]