"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import TemplateView


urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/', include('story9.urls'), name='accounts'),
    path('', TemplateView.as_view(template_name='buku.html'), name='buku'),
    path('', include('story3.urls')),
    path('story-3/', include('story1.urls')),
    path('schedule/', include('jadwal.urls')),
    path('activity/', include('activity.urls')),
    path('story7/', include('story7.urls')),
    path('', include('story8.urls'))
    
]
