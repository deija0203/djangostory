from django.urls import path
from . import views

app_name = 'story3'

urlpatterns = [
    path('', views.buku, name='buku'),
    path('profile/', views.index, name='index'),
   
]