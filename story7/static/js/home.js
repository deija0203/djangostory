$(document).ready(function() {

  $('.accordion').click(function(){
    if($(this).parent().hasClass('active')){
      $(this).parent().removeClass('active');
    }
    else {
      $('.item').removeClass('active');
      $(this).parent().addClass("active")
    }
  })

  $('button').click(function(event){
    event.preventDefault();
    var parent = $(this).closest('div');
    if($(this).hasClass('btn-up')) {
      parent.insertBefore(parent.prev('div'));
    }
    else if($(this).hasClass('btn-down')){
      parent.insertAfter(parent.next());
    }
  })

});