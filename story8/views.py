from django.shortcuts import redirect, render
from django.http import HttpResponse, JsonResponse
import requests


# Create your views here.

def book_search(request):
    return render(request, 'book_search.html')

def book_data(request):
    buku = request.GET['buku']
    url_tujuan = 'https://www.googleapis.com/books/v1/volumes?q=' + buku
    # r = requests.get(url_tujuan)
    # data = json.loads(r.content)
    result = requests.get(url_tujuan)
    data = result.json()
    return JsonResponse(data, safe=False)
