$(document).ready(function() {

    $.ajax({
        method : 'GET',
        url : "/data?book=ajax/",
        success : function(hasil) {
            console.log(hasil.items)
            
            var result = "";
            for (let i = 0; i < hasil.items.length; i++) {
                result += ('<tr>' + '<td> <a id="judul" href="' + hasil.items[i].volumeInfo.canonicalVolumeLink + '">' + hasil.items[i].volumeInfo.title + ' </a> </td>' + '<td>' + hasil.items[i].volumeInfo.authors + '</td>' + '<td>')

                if (hasil.items[i].volumeInfo.description != undefined) {
                    if (hasil.items[i].volumeInfo.description.length > 230) {
                        result += hasil.items[i].volumeInfo.description.substring(0,230) + "..." + '</td>'
                    }
                    else {
                        result += hasil.items[i].volumeInfo.description + '</td>'
                    }
                } 
                else {
                    result += "---";
                }

                if (hasil.items[i].volumeInfo.imageLinks != null) {
                    result += ('<td> <img src=' + hasil.items[i].volumeInfo.imageLinks.smallThumbnail + '></td>' + '</tr>')
                }
                else {
                    result += ('<td> <p> --- </p> </td>' + '</tr>')
                }
            }
            $('#hasil').append(result);
            $('#hasil').append('</table>');
        }
    })

    $("#keyword").keyup(function(e) {
        if (e.keyCode === 13) {
            e.preventDefault();
            $("#search").click();
        }
    });

    $("#search").click(function() {
        var book = $("#keyword").val();
        console.log(book);
        var books_url = "https://www.googleapis.com/books/v1/volumes?q=" + book;
        console.log(books_url);

        $.ajax({
            method : 'GET',
            url : "/data?book=" + book + "/",
            success : function(hasil) {
                console.log(hasil.items)
                $("#hasil").empty();
                
                var result = "";
                for (let i = 0; i < hasil.items.length; i++) {
                    result += ('<tr>' + '<td> <a id="judul" href="' + hasil.items[i].volumeInfo.canonicalVolumeLink + '">' + hasil.items[i].volumeInfo.title + ' </a> </td>' + '<td>' + hasil.items[i].volumeInfo.authors + '</td>' + '<td>')

                    if (hasil.items[i].volumeInfo.description != undefined) {
                        if (hasil.items[i].volumeInfo.description.length > 230) {
                            result += hasil.items[i].volumeInfo.description.substring(0,230) + "..." + '</td>'
                        }
                        else {
                            result += hasil.items[i].volumeInfo.description + '</td>'
                        }
                    } 
                    else {
                        result += "---";
                    }

                    if (hasil.items[i].volumeInfo.imageLinks != null) {
                        result += ('<td> <img src=' + hasil.items[i].volumeInfo.imageLinks.smallThumbnail + '></td>' + '</tr>')
                    }
                    else {
                        result += ('<td> <p> --- </p> </td>' + '</tr>')
                    }
                }
                $('#hasil').append(result);
                $('#hasil').append('</table>');
            }
        })
    });
});

