from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Activity, Participant
from .forms import ActivityForm, ParticipantForm

# Create your views here.
def add_activity(request):
    form = ActivityForm()
    if request.method == 'POST':
        inputform = ActivityForm(request.POST)
        if inputform.is_valid():
            inputform.save()
            return render(request, 'add_activity.html',{'form': form,'status':'success', 'data' : Activity.objects.all()})
        else :
            return render(request, 'add_activity.html',{'form':form, 'status':'failed', 'data': Activity.objects.all()})
    else :
        current_data = Activity.objects.all()
        return render(request, 'add_activity.html', {'form':form, 'data':current_data})

def detail_activity(request):
    form = ParticipantForm()
    act_data = Activity.objects.all()
    ptc_data = Participant.objects.all()
    if request.method == 'POST':
        inputform = ParticipantForm(request.POST)
        if inputform.is_valid():
            inputform.save()
            return redirect(detail_activity)
        else:
            participantData = Participant.objects.all()
            return render(request, 'detail_activity.html', {'form':form, 'status': 'failed', 'participantData' : ptc_data, 'activityData': act_data })
    else:
        return render(request,'detail_activity.html', {'form':form, 'status': 'failed', 'participantData' : ptc_data, 'activityData': act_data})
