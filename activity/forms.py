from django import forms
from .models import Activity, Participant

class ActivityForm(forms.ModelForm):
    class Meta :
        model = Activity
        fields = '__all__'
        # widgets = {
        #     'activity': forms.Select(attrs={'class':'form-control'})
        # }

class ParticipantForm(forms.ModelForm):
    class Meta :
        model = Participant
        fields = '__all__'
        # widgets = {
        #     'activity': forms.Select(attrs={'class':'form-control'})
        #     "name" : forms.TextInput(attrs={'class':'form-control'})
        # }