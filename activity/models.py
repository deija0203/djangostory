from django.db import models


# Create your models here.
class Activity(models.Model):
    activity = models.CharField('Activity', max_length=100)

    # class Meta:
    #     db_table = 'activity'

    def __str__(self):
         return self.activity


class Participant(models.Model):
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE) #link 2 tables together
    name = models.CharField('Partisipan', max_length=50)

    # #class Meta:
    #     db_table = 'participant'

    def __str__(self):
        return self.name

