from django.contrib import admin
from django.urls import path, include
from . import views


urlpatterns =[
    path('activity/',views.add_activity, name='add_activity' ),
    path('data/', views.detail_activity, name='detail_activity'),
]