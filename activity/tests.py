from django.test import LiveServerTestCase, TestCase, tag, Client
from .models import Activity, Participant
from .forms import ActivityForm, ParticipantForm
from .views import add_activity, detail_activity
from django.http import HttpRequest
from django.urls import reverse, resolve
from selenium import webdriver

# Create your tests here.
class TestActivityAddUnitTest(TestCase):

    def test_activity_url_is_exist(self):
        response = Client().get("/activity/activity/")
        self.assertEquals(200, response.status_code)

    def test_template_form(self):
        response = Client().get("/activity/activity/")
        self.assertTemplateUsed(response, "add_activity.html")

    def test_view_form(self):
        response = Client().get("/activity/activity/")
        html_response = response.content.decode("utf8")
        self.assertIn('<h3 style="font-size: 55px;">Add Activity</h3>', html_response)
        self.assertIn('<form method="POST">', html_response)
    

    def test_model_create(self):
        Activity.objects.create(activity="cobaaa")
        count = Activity.objects.all().count()
        self.assertEquals(count, 1)

    def test_model_name(self):
        Activity.objects.create(activity='check')
        activity = Activity.objects.get(activity='check')
        self.assertEqual(str(activity),'check')

    def test_model_relationship(self):
        Activity.objects.create(activity = "test")
        activity_id = Activity.objects.all()[0].id
        activity_id_2 = Activity.objects.get(id=activity_id)
        Participant.objects.create(activity = activity_id_2, name="ttt")
        count = Participant.objects.filter(activity=activity_id_2).count()
        self.assertEquals(count, 1)

    def test_form_validation_accepted(self):
        form = ActivityForm(data={'activity': 'test'})
        self.assertTrue(form.is_valid())
    
    def test_model_name2(self):
        Activity.objects.create(activity='naon')
        id_par = Activity.objects.all()[0].id
        activity = Activity.objects.get(id=id_par)
        Participant.objects.create(activity=activity, name='testtest')
        participant = Participant.objects.get(activity=activity)
        self.assertEqual(str(participant),'testtest')

class TestActivityDetailsUnitTest(TestCase):

    def test_activity_url_is_exist(self):
        response = Client().get("/activity/data/")
        self.assertEquals(200, response.status_code)

    def test_template_form(self):
        response = Client().get("/activity/data/")
        self.assertTemplateUsed(response, "detail_activity.html")

    def test_model_create(self):
        activity_1 = Activity.objects.create(activity="testbang")
        Participant.objects.create(activity = activity_1, name="test")
        count = Participant.objects.all().count()
        self.assertEquals(count, 1)

    def test_new_participant_form_validation(self):
        activity_new = Activity.objects.create(activity = "testt")
        form = ParticipantForm(data = {'activity' :activity_new, 'name' : 'Unittesting'})
        self.assertTrue(form.is_valid())

    def test_data_view_show(self):
        Activity.objects.create(activity='Test')
        request = HttpRequest()
        response = detail_activity(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Test', html_response)
    
    def test_insert_blank_participant(self):
        activity_new = Activity.objects.create(activity = "coba")
        form = ParticipantForm(data = {'activity' :activity_new, 'name' : ''})
        self.assertEqual(form.errors['name'], ["This field is required."])

    def test_activity_add_using_func(self):
        found = resolve('/activity/activity/')
        self.assertEqual(found.func, add_activity)

@tag('functional')
class IndexFunctionalTest(LiveServerTestCase):
    
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


